I"<h2 id="1-inferring-functions">1. Inferring Functions</h2>

<p>Consider our model of function inference from the chapter:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>///fold:
// make expressions easier to look at
var prettify = function(e) {
  if (e == 'x' || _.isNumber(e)) {
    return e
  } else {
    var op = e[0]
    var arg1 = prettify(e[1])
    var prettyarg1 = (!_.isArray(e[1]) ? arg1 : '(' + arg1 + ')')
    var arg2 = prettify(e[2])
    var prettyarg2 = (!_.isArray(e[2]) ? arg2 : '(' + arg2 + ')')
    return prettyarg1 + ' ' + op + ' ' + prettyarg2
  }
}

var plus = function(a,b) {
  return a + b;
}

var multiply = function(a,b) {
  return Math.round(a * b,0);
}

var divide = function(a,b) {
  return Math.round(a/b,0);
}

var minus = function(a,b) {
  return a - b;
}

var power = function(a,b) {
  return Math.pow(a,b);
}

// make expressions runnable
var runify = function(e) {
  if (e == 'x') {
    return function(z) { return z }
  } else if (_.isNumber(e)) {
    return function(z) { return e }
  } else {
    var op = (e[0] == '+') ? plus : 
             (e[0] == '-') ? minus :
             (e[0] == '*') ? multiply :
             (e[0] == '/') ? divide :
              power;
    var arg1Fn = runify(e[1])
    var arg2Fn = runify(e[2])
    return function(z) {
      return op(arg1Fn(z),arg2Fn(z))
    }
  }
}

var randomConstantFunction = function() {
  return uniformDraw(_.range(10))
}

var randomCombination = function(f,g) {
  var op = uniformDraw(['+','-','*','/','^']);
  return [op, f, g];
}

// sample an arithmetic expression
var randomArithmeticExpression = function() {
  if (flip(0.3)) {
    return randomCombination(randomArithmeticExpression(), randomArithmeticExpression())
  } else {
    if (flip()) {
      return 'x'
    } else {
      return randomConstantFunction()
    }
  }
}
///

viz.table(Infer({method: 'enumerate', maxExecutions: 100}, function() {
  var e = randomArithmeticExpression();
  var s = prettify(e);
  var f = runify(e);
  
  condition(f(0) == 0)
  condition(f(2) == 4)
  
  return {s: s};
}))
</code></pre></div></div>

<h4 id="a">a)</h4>

<p>Why does this think the probability of <code class="language-plaintext highlighter-rouge">x * 2</code> is so much lower than <code class="language-plaintext highlighter-rouge">x * x</code>?</p>

<p>HINT: Think about the probability assigned to <code class="language-plaintext highlighter-rouge">x ^ 2</code>.</p>

<h4 id="b">b)</h4>

<p>Let’s reconceptualize our program as a sequence-generator by making the input arguments \(1,2,3,\dots\). Suppose that the first number in the sequence (\(f(1)\)) is <code class="language-plaintext highlighter-rouge">1</code> and the second number (\(f(2)\)) is <code class="language-plaintext highlighter-rouge">4</code>. What number comes next?</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>///fold:
// make expressions easier to look at
var prettify = function(e) {
  if (e == 'x' || _.isNumber(e)) {
    return e
  } else {
    var op = e[0]
    var arg1 = prettify(e[1])
    var prettyarg1 = (!_.isArray(e[1]) ? arg1 : '(' + arg1 + ')')
    var arg2 = prettify(e[2])
    var prettyarg2 = (!_.isArray(e[2]) ? arg2 : '(' + arg2 + ')')
    return prettyarg1 + ' ' + op + ' ' + prettyarg2
  }
}

var plus = function(a,b) {
  return a + b;
}

var multiply = function(a,b) {
  return Math.round(a * b,0);
}

var divide = function(a,b) {
  return Math.round(a/b,0);
}

var minus = function(a,b) {
  return a - b;
}

var power = function(a,b) {
  return Math.pow(a,b);
}

// make expressions runnable
var runify = function(e) {
  if (e == 'x') {
    return function(z) { return z }
  } else if (_.isNumber(e)) {
    return function(z) { return e }
  } else {
    var op = (e[0] == '+') ? plus : 
             (e[0] == '-') ? minus :
             (e[0] == '*') ? multiply :
             (e[0] == '/') ? divide :
              power;
    var arg1Fn = runify(e[1])
    var arg2Fn = runify(e[2])
    return function(z) {
      return op(arg1Fn(z),arg2Fn(z))
    }
  }
}

var randomConstantFunction = function() {
  return uniformDraw(_.range(10))
}

var randomCombination = function(f,g) {
  var op = uniformDraw(['+','-','*','/','^']);
  return [op, f, g];
}

// sample an arithmetic expression
var randomArithmeticExpression = function() {
  if (flip(0.3)) {
    return randomCombination(randomArithmeticExpression(), randomArithmeticExpression())
  } else {
    if (flip()) {
      return 'x'
    } else {
      return randomConstantFunction()
    }
  }
}
///

viz.table(Infer({method: 'enumerate', maxExecutions: 10000}, function() {
  var e = randomArithmeticExpression();
  var s = prettify(e);
  var f = runify(e);
  
  condition(f(1) == 1)
  condition(f(2) == 4)
  
  return {'f(3)':f(3)};
}))
</code></pre></div></div>

<p>Not surprisingly, the model predicts <code class="language-plaintext highlighter-rouge">9</code> as the most likely next number. However, it also puts significant probability on <code class="language-plaintext highlighter-rouge">27</code>. Why does this happen?</p>

<h4 id="c">c)</h4>

<p>Many people find the high probability assignmed by our model in (b) to <code class="language-plaintext highlighter-rouge">27</code> to be unintuitive (i.e. if we ran this as an experiment, 27 would be a very infrequent response). This suggests our model is an imperfect model of human intuitions. How could we decrease the probability of inferring <code class="language-plaintext highlighter-rouge">27</code>? (HINT: Consider the priors).</p>

<h2 id="2-role-governed-concepts-challenge">2. Role-governed concepts (challenge!)</h2>

<p>In the Rational Rules model we saw in the chapter, concepts were defined in terms of the features of single objects (e.g. “it’s a raven if it has black wings”). Psychologists have suggested that many concepts are not defined by the features of a single objects, but instead by the relations the object has to other objects. For instance, “a key is something that opens a lock”. These are called <em>role-governed</em> concepts.</p>

<p>Extend the Rational Rules model to capture role-governed concepts.</p>

<p>Hint: You will need primitive relations in your langauge of thought.</p>

<p>Hint: Consider adding quantifiers (e.g. <em>there exists</em>) to your language of thought.</p>
:ET