I"�<h2 id="exercise-1-factors">Exercise 1: Factors</h2>

<h3 id="a">a)</h3>

<p>Take our standard coin-flipping model. Use <code class="language-plaintext highlighter-rouge">factor</code> to create a “soft” condition on the outcome being heads, such that there is an approx. 95% chance of heads.</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var dist = Infer({method: 'enumerate'},
  function () {
    var A = flip()
    factor(A) //edit this line
    return A
});
viz(dist)
</code></pre></div></div>

<h3 id="b">b)</h3>

<p>In this model, we flip 3 coins. Use <code class="language-plaintext highlighter-rouge">factor</code> to favor an outcome of 2 heads and 1 tails:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var softHeads = Infer({ 
  model() {
    var a = flip(0.5);
    var b = flip(0.5);
    var c = flip(0.5);
    factor( \\your code here );
    return a;
  }
});

viz(softHeads);
</code></pre></div></div>

<h2 id="exercise-2-the-ultimatum-game">Exercise 2: The Ultimatum Game</h2>

<h3 id="a-1">a)</h3>

<p>The ultimatum game requires two players: A proposer and a responder. The proposer has to decide how to allocate $10 between the two players in $1 increments. Once this proposal is made, the responder decides whether to accept the proposal. If the responder accepts, both players are awarded the money according to the proposal. If the responder rejects, neither player gets anything.</p>

<p>If the responder was a strict utilitarian, s/he would accept any offer of $1 or more. Assume the proposer is a soft maximizer who wants to keep as much of the $10 as possible. Complete the code below to find out how much the proposer will offer:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>
var responder = function(offer) {    
    
    // your code here
    
}

var proposer = Infer({method: "enumerate"}, function(){
	
	// your code here
	
	factor(reward)
	return(offer)	
	})

viz(proposer);
</code></pre></div></div>

<h3 id="b-1">b)</h3>

<p>People, it turns out, act very differently than the model above suggests. Responders will often reject low offers as “unfair”, even though this means they get nothing. Assume that the responder decides whether to accept in proportion to the percentage of the $10 allocated to her, raised to some power <code class="language-plaintext highlighter-rouge">alpha</code> (you can think of <code class="language-plaintext highlighter-rouge">alpha</code> as “spitefulness”). Complete the code below to determine how much the proposer should offer:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>
var responder = function(offer, alpha) {    
  var p = Math.pow(offer/10,alpha)
	return(flip(p));
}

var proposer = Infer({method: "enumerate"}, function(){
	
	// your code here
	
	factor(reward)
	return(offer)	
	})

viz(proposer);
</code></pre></div></div>

<h3 id="c">c)</h3>

<p>You can think of the variable <code class="language-plaintext highlighter-rouge">alpha</code> in the code above as encoding spitefulness: the degree to which the responder is willing to forego a reward in order to prevent the proposer from having a reward. See how setting <code class="language-plaintext highlighter-rouge">alpha</code> to 4, 6, 10, 25, and 50 affects what the proposer does. Explain the results.</p>

<h3 id="d">d)</h3>

<p>The models above assume the proposer knows the responder’s decision function. Let’s soften that assumption: the proposer knows that the responder’s value of <code class="language-plaintext highlighter-rouge">alpha</code> is somewhere on the range [0.5, 5]. Suppose the proposer offer $2 and the responder rejects it. What is the most likely level of <code class="language-plaintext highlighter-rouge">alpha</code>? How does that change if the first offer was $8?</p>

<p>(Hint: you may find it helpful to find a different place for <code class="language-plaintext highlighter-rouge">alpha</code> than within the definition of <code class="language-plaintext highlighter-rouge">responder</code>.)</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var responder = function(offer, alpha) {    

	// your code here

}

var proposer = Infer({method: "MCMC", samples:50000}, function(){

	// your code here

]})

viz(proposer)
</code></pre></div></div>

<h3 id="e">e)</h3>

<p>Extend the model in (d) as follows: Suppose the proposer and responder are going to play twice. Does it ever make sense for the responder to reject the first proposal in order to increase the total expected payoff across the two games? (If you cannot figure out how to write the model, a verbal description is OK.)</p>

<h2 id="exercise-3-the-prisoners-dilemma">Exercise 3: The Prisoner’s Dilemma</h2>

<p>In the prisoner’s dilemma, two thieves work together on a bank heist. Afterwards, they are apprehended by the police. The police interrogate the thieves separately. They tell each thief that if she confesses, she will get a lenient sentence. If not, she will get 10 years. However, the thieves know that the police need at least one of them to confess; if neither of them confesses, the police don’t have enough evidence to charge them, and they will both go free.</p>

<p>What’s the longest the lenient sentence can be (in round years) such that it makes sense for the thief to confess (that is, where she has a greater than 50% chance of confessing)? Use <code class="language-plaintext highlighter-rouge">factor(percentYearsFreedom)</code> where <code class="language-plaintext highlighter-rouge">percentYearsFreedom</code> is the percentage of the next 10 years the thief will not be in jail. (Assume that this incident has scared her straight and she will not commit any other crimes.)</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>var thiefRats = function(){
  return (flip()? true: false)
}

var thief = Infer({}, function(){

	// your code here

})

viz(thief)
</code></pre></div></div>

<h2 id="exercise-4-exploring-rsa">Exercise 4: Exploring RSA</h2>

<p>For this exercise, modify the RSA model introduced in the main text as necessary.</p>

<h3 id="a-2">a)</h3>

<p>How does increasing the optimality of the speaker affect the pragmatic listener’s inferences? Try a couple values and report the results.</p>

<h3 id="b-2">b)</h3>

<p>How do the inferences of \(L_{2}\) compare to those of \(L_{1}\)?</p>

<h3 id="c-1">c)</h3>

<p>Add a green circle to the scenario. What happens to the interpretion of “blue”? Why?</p>

<h3 id="d-1">d)</h3>

<p>Is there any way to get “blue” to refer to something green? Why or why not?</p>
:ET