---
layout: exercise
title: Riley Holterhus PPL Assignment
custom_js:
- assets/js/box2d.js
- assets/js/physics.js
---

~~~~
var degreesToRadians = function(degrees) {
  return (3.14159 / 180) * degrees;
}

var addRamp = function(world, degrees) {
  var l = 60;
  var w = 3;
  var x = 150 - (l * (degrees / 180));
  var y = 640 + (l * (degrees / 180));
  world.push({shape: 'rect', static: true, dims: [w, l], x: x, y: y, angle: degreesToRadians(degrees)});
  return world;
}

var addBucket = function(world) {
  world.push({shape: 'rect', static: true, dims: [3,30], x: 500, y: 500});
  world.push({shape: 'rect', static: true, dims: [60,3], x: 560, y: 530});
  world.push({shape: 'rect', static: true, dims: [3,30], x: 620, y: 500});
  return world;
}

var addBall = function(world,v) {
  world.push({shape: 'circle', static: false, dims: [5], x: 5, y: 665, velocity: [v,0]});
  return world;
}

var isInBox = function(finalWorld) {
  var withinBounds = function(obj) { 
    return obj.shape == "circle" && obj.x <= 620 && obj.x >= 400 && obj.y <= 550; 
  }
  return any(withinBounds, finalWorld);
}

var getWorld = function(vel,deg) {
  return addRamp(addBall(addBucket([{shape: 'rect', static: true, dims: [1000,30], x: 0, y: 700}]),vel), deg);
}

var model = function() {
  var vel = sample(Uniform({a: 800, b: 3000}));
  var deg = sample(RandomInteger({n:90}));
  var finalWorld = physics.run(1000, getWorld(vel,deg));
  condition(isInBox(finalWorld));
  return {vel: vel, deg: deg};
}
var dist = Infer({method: 'MCMC',
     samples: 500,
    }, model)
editor.put("savedDist", dist);
editor.put("addBucket", addBucket);
editor.put("addBall", addBall);
editor.put("addRamp", addRamp);
~~~~

~~~~
var dist = editor.get("savedDist");
var addBucket = editor.get("addBucket");
var addBall = editor.get("addBall");
var addRamp = editor.get("addRamp");

viz(dist);

var trySample = function(v, d) {
  var startWorld = [{shape: 'rect', static: true, dims: [1000,30], x: 0, y: 700}];
  var world = addRamp(addBall(addBucket(startWorld, 0, 0),v), d);
  physics.animate(1000, world);
}

var conditionDegree = function() {
  var s = sample(dist);
//   condition(s.deg >=80);
  return s;
}

var s = sample(Infer(conditionDegree));
trySample(s.vel, s.deg);
~~~~

