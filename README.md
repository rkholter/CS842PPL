<h1>Riley Holterhus PPL Assignment<h1>

<h4>Here are three gifs I took from the animation of the physics. Each one of these gifs was obtained by conditioning the ramp to have a certain angle in the program. There was a small bug in the program where a ramp with very low angle has a sharp angle at the very start since it is clipped outisde of the floor (look at the 2nd gif), but since this looked pretty interesting I left this in.  These gifs are in the Images folder.</h4>


<img src="Images/gif1.gif" width="300" height="300"/>
<img src="Images/gif2.gif" width="300" height="300"/>
<img src="Images/gif3.gif" width="300" height="300"/>



<h4>The source code of this is in the src folder, however most of the code there was just for me to get WebPPL set-up on github pages with the physics engine, so that I could write different code snippets in the WebPPL browser editor I forked from https://probmods.org/. As a result, I believe the best way to look at the source code is to visit the web editor at https://rholterhus.github.io/CS842PPL/, but the main source code could also bee seen in the index.md file in the src folder</h4>
